# Zoomies 

## Summary

A first-person platformer made with the Godot game engine with a focus on creative movement especially featuring slippery surfaces. This simple game will form the foundation for more complicated titles in the future. The code for the game will focus on simplicity and readability so that it can be easily reused by myself and others.

This game will be seperated into 10 levels. At the start of each level, the player will be given 30 seconds to reach the level's goal. Across the level will be icons to collect that will add an additional second onto the timer. Each level will have 60 of these bringing the total to 90 seconds per level or 15 minutes for the whole game.

The game will have a medieval fantasy theme with a bright, happy mood. The castle structure will be mostly flooded with flowing water along stone walls and large pools of water. Most levels will focus on climbing vertically to a goal at the highest possible point. 

## Documentation
This section will contain links to various parts of the docs.

* [Work Log](docs/work_log.md) Journal of development progress.
* [Detailed Design](docs/design_doc.md) Full documentation of game features and goals.
