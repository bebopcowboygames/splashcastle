extends Area3D


# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("rotate")


func _on_body_entered(body):
	get_tree().call_group("special_object_manager", "time_orb_collected")
	hide()
	queue_free()
