extends Control

const win_sound = "res://resources/sounds/Catgirl Fighting SFX/congratulations/001_congratulations.wav"
const lose_sound = "res://resources/sounds/Catgirl Fighting SFX/we lost/003_we-lost.wav"

var fullscreen_mode = false
var menu_is_open = true
var main_player_character
var original_position
@onready var current_level = get_parent().name.trim_prefix("Level_").to_int()
@onready var specialobjects = $"../SpecialObjects"

enum {PAUSE_MENU, LEVEL_SELECT, START_MENU, LEVEL_FINISH, GAME_START}

const levels = {1: "res://levels/level_1.tscn",
				2: "res://levels/level_2.tscn",
				3: "res://levels/level_3.tscn",
				4: "res://levels/level_4.tscn",
				5: "res://levels/demo_end.tscn",
				"test": "res://levels/test_level.tscn"}

const level_names = {
	1: "Level 1: Getting Started",
	2: "Level 2: Falling Blocks",
	3: "Level 3: Running on Walls",
	4: "Level 4: Slippery Slopes"
}

func _ready():
	main_player_character = get_player()
	original_position = main_player_character.transform
	
	var label = $Level_Start_Menu/MarginContainer/VBoxContainer/Level_Name
	label.text = level_names[current_level]
	
	level_1_check()
	
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	check_for_keys()

func check_for_keys():
	if Input.is_action_just_pressed("fullscreen"):
		toggle_fullscreen()
	if Input.is_action_just_pressed("menu_open_close"):
		menu_open_close()
#	if Input.is_action_just_pressed("reset_player"):
#		reset_player()
#	if Input.is_action_just_pressed("quit"):
#		get_tree().quit()

func toggle_fullscreen():
	if fullscreen_mode:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		show_mouse()
		fullscreen_mode = false
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
		if not menu_is_open:
			grab_mouse()
		fullscreen_mode = true

func menu_open_close():
	var timer = $"../SpecialObjects/MainLevelTimer"
	if menu_is_open:
		hide()
		grab_mouse()
		main_player_character.paused = false
		timer.paused = false
		menu_is_open = false
	else:
		show()
		open_menu(PAUSE_MENU)
		show_mouse()
		main_player_character.paused = true
		timer.paused = true
		menu_is_open = true

func grab_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func show_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func get_player():
	var parent = get_parent()
	var player = parent.find_child("Player")
	return player

func open_menu(menu):
	var pause = $Pause_Menu
	pause.hide()
	var level = $Level_Select
	level.hide()
	var start = $Level_Start_Menu
	start.hide()
	var finish = $Level_Finish
	finish.hide()
	var game_start = $First_start_screen
	game_start.hide()
	
	match menu:
		PAUSE_MENU:
			pause.show()
		LEVEL_SELECT:
			level.show()
		LEVEL_FINISH:
			finish.show()
		START_MENU:
			start.show()
		GAME_START:
			game_start.show()

func _on_button_resume_pressed():
	menu_open_close()

func _on_button_fullscreen_pressed():
	toggle_fullscreen()

func _on_button_quit_pressed():
	get_tree().quit()


func _on_button_levels_pressed():
	open_menu(LEVEL_SELECT)


func _on_button_test_level_pressed():
	var testlevel = "res://levels/test_level.tscn"
	get_tree().change_scene_to_file(testlevel)

func _on_button_level_1_pressed():
	var level1 = "res://levels/level_1.tscn"
	get_tree().change_scene_to_file(level1)

func _on_button_level_2_pressed():
	var level2 = "res://levels/level_2.tscn"
	get_tree().change_scene_to_file(level2)

func reset_player():
	main_player_character.transform = original_position


func _on_button_level_3_pressed():
	var level3 = "res://levels/level_3.tscn"
	get_tree().change_scene_to_file(level3)


func _on_button_level_4_pressed():
	var level4 = "res://levels/level_4.tscn"
	get_tree().change_scene_to_file(level4)

func finish_screen():
	main_player_character.paused = true
	adjust_finish_screen()
	show()
	open_menu(LEVEL_FINISH)
	show_mouse()
	menu_is_open = true
	$AudioStreamPlayer.play()

func adjust_finish_screen():
	var timeLabel = $Level_Finish/MarginContainer/VBoxContainer/MarginContainer2/TimeLeft
	var passLabel = $Level_Finish/MarginContainer/VBoxContainer/MarginContainer3/Pass_Fail_Label
	var nextButton = $Level_Finish/MarginContainer/VBoxContainer/Button_Next_Skip
	var timer = specialobjects.find_child("MainLevelTimer")
	var time = timer.time_left
	var timestring = str(time).pad_decimals(1)

	timeLabel.text = "Time Left: " + timestring
	if(time > 0):
		passLabel.text = "Congratulations!
						\nYou finished the level
						\nwith time to spare!"
		nextButton.text = "Next"
		$AudioStreamPlayer.stream = load(win_sound)
	else:
		passLabel.text = "Oh no!
						\nYou ran out of time.
						\nTry again or skip level?"
		nextButton.text = "Skip"
		$AudioStreamPlayer.stream = load(lose_sound)


func _on_button_next_skip_pressed():
	load_next_level()

func load_next_level():
	var n = current_level + 1
	var level = levels[n]
	get_tree().change_scene_to_file(level)

func _on_button_retry_pressed():
	get_tree().reload_current_scene()


func _on_button_begin_pressed():
	menu_open_close()

func level_1_check():
	var firstload = get_node("/root/Gametracker").firstLoad
	var levelone = get_parent().name == "Level_1"
	if(firstload and levelone):
		open_menu(GAME_START)
		get_node("/root/Gametracker").firstLoad	= false

func _on_button_start_game_pressed():
	open_menu(START_MENU)


func _on_button_fullscreen_start_pressed():
	toggle_fullscreen()
