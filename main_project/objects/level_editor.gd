extends Node3D

const level_1_path = "res://levels/level_1.tscn"
const wall_block_path = "res://objects/block_wall.tscn"

var level_1
var wall_block_resource : PackedScene
# Called when the node enters the scene tree for the first time.
func _ready():
	edit_level_1()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func edit_level_1():
	load_level_1()
	change_blocks()
	
func load_level_1():
	var level_1_resource = preload(level_1_path)
	level_1 = level_1_resource.instantiate()
	add_child(level_1)
	
func change_blocks():
	var type = "StaticBody3D"
	var recursive = true
	var not_owned = false
	var terrain = level_1.find_child("Terrain")
	var all_blocks = Array()
	for child in terrain.get_children():
		if not (child.name.match("Obstacles*")):
			all_blocks.append_array(child.find_children("Block*"))
#	var bottom = terrain.find_child("Bottom_Floor")
#	print_debug(str(bottom))
#	var all_blocks = bottom.find_children("*Block*", type, recursive, not_owned)
#	var Lev1 = find_child("Level_1", recursive, not_owned)
##	print_debug(str(Lev1))
#	var terrain = Lev1.find_child("Terrain")
#	var bottom_floor = terrain.find_child("Bottom_Floor")
#	all_blocks = bottom_floor.find_children("Block*")
#	print_debug(str(all_blocks))
	
	wall_block_resource = load(wall_block_path)
	print_debug(str(all_blocks))
	for block in all_blocks:
		change_out(block)
		
func save_level_1():
	var scene = PackedScene.new()
	scene.pack(level_1)
	ResourceSaver.save(scene, "res://script_out/level_1_new.tscn")

func change_out(old_block : StaticBody3D):
	var new_block = wall_block_resource.instantiate()
	new_block.transform = old_block.transform
	var parent = old_block.get_parent()
	parent.add_child(new_block)
	new_block.owner = level_1
	old_block.queue_free()


func _on_end_timer_timeout():
	save_level_1()
	get_tree().quit()
