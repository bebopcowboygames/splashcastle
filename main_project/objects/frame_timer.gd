extends Node
@export var wait_frames = 10
var frames_left = 0

func _physics_process(delta):
	if not is_stopped():
		frames_left -= 1

func is_stopped():
	if frames_left <= 0:
		return true
	else:
		return false

func start():
	frames_left = wait_frames

