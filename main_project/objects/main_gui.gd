extends PanelContainer

var fullscreen_mode = false
var menu_is_open = true
var main_player_character

func _ready():
	main_player_character = get_player()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	check_for_keys()

func check_for_keys():
	if Input.is_action_just_pressed("fullscreen"):
		toggle_fullscreen()
	if Input.is_action_just_pressed("menu_open_close"):
		menu_open_close()
	if Input.is_action_just_pressed("quit"):
		get_tree().quit()

func toggle_fullscreen():
	if fullscreen_mode:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		show_mouse()
		fullscreen_mode = false
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
		if not menu_is_open:
			grab_mouse()
		fullscreen_mode = true

func menu_open_close():
	if menu_is_open:
		hide()
		grab_mouse()
		main_player_character.paused = false
		menu_is_open = false
	else:
		show()
		show_mouse()
		main_player_character.paused = true
		menu_is_open = true

func grab_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func show_mouse():
	Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func get_player():
	var parent = get_parent()
	var player = parent.find_child("Player")
	return player


func _on_button_start_pressed():
	menu_open_close()

func _on_button_fullscreeen_pressed():
	toggle_fullscreen()


func _on_button_quit_pressed():
	get_tree().quit()
