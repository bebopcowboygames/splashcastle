extends CharacterBody3D

var jump_velocity = 6
var fall_threshold = -100.0
var basic_friction = 10.0
var walk_speed = 24.0
var run_speed = 16.0
var air_speed = 24.0

var max_air = 3.0
var max_walk = 4.0
var max_run = 12.0

var mouse_x_sensitivity = 1.0 / 50000.0
var mouse_y_sensitivity = 1.0 / 50000.0

var paused = true
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var standing_on = ""

var the_floor

func _physics_process(delta):
	if not paused:
		#Not movement related
		out_of_bounds_check()
		look()
		
		#affect velocity
		apply_gravity(delta)
		walk_or_run(delta)
		apply_friction(delta)
		jump()
		
		#perform movement
		move_and_slide()

func look():
	var mouse_move = Input.get_last_mouse_velocity()
	var cam = $Camera3D
	rotate_y(mouse_move.x * mouse_x_sensitivity * -1)
	cam.rotate_x(mouse_move.y * mouse_y_sensitivity * -1)

func walk_or_run(delta):
	var input_dir = Input.get_vector("left_strafe","right_strafe","forward","backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	var new_velocity = velocity

	if not direction.length() > 0:
		return #no input
	
	#debug
	var h_velocity = Vector2(velocity.x, velocity.z).length()
	get_tree().call_group("debug_handler", "update_hvel", h_velocity)

	if not is_on_floor():
		#in the air
		new_velocity = velocity + direction	* air_speed * delta
		if h_speed(new_velocity) < max_air:
			velocity = new_velocity
			return
	else:
		#on the ground
		new_velocity = velocity + direction * walk_speed * delta
		if h_speed(new_velocity) < max_walk:
			velocity = new_velocity
			return
		new_velocity = velocity + direction * run_speed * delta
		if h_speed(new_velocity) < max_run:
			velocity = new_velocity
			return

func jump():
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y += jump_velocity

func apply_gravity(delta):
	var low_gravity = gravity / 2.0
	if is_on_floor():
		velocity.y -= low_gravity * delta
	else:
		velocity.y -= gravity * delta
		
func apply_friction(delta):
	#IN_PROGRESS!!
	
	# Not touching floor
	if not is_on_floor():
		return
		
	# Moving
	if velocity.length() <= 0:
		return
		
	var floor_friction = get_floor_friction() * delta
	
	var friction_vector = Vector2(velocity.x, velocity.z)
	
	friction_vector = friction_vector.move_toward(Vector2.ZERO, floor_friction)
	
	velocity.x = friction_vector.x
	velocity.z = friction_vector.y

func out_of_bounds_check():
	if position.y < fall_threshold:
		print_debug("out of bounds")
		get_tree().quit()

func get_floor_friction_old():
	var friction = basic_friction
	
	var collision = get_last_slide_collision()
	if not collision is KinematicCollision3D:
		return friction
	
	var collider = collision.get_collider()
	if collider is Node:
		standing_on = collider.name
	if not collider is StaticBody3D:
		return friction
	
	var material = collider.physics_material_override
	if not material is PhysicsMaterial:
		return friction
	
	#finally
	friction = friction * material.friction * 2
	return friction

func get_floor_friction():
	var friction = basic_friction
	
	if the_floor is Node:
		standing_on = the_floor.name
	if not the_floor is StaticBody3D:
		return friction
	
	var material = the_floor.physics_material_override
	if not material is PhysicsMaterial:
		return friction
	
	#finally
	friction = friction * material.friction * 2
	return friction
	

func _on_feet_body_entered(body):
	the_floor = body

func h_speed(vector_in):
	var vector_out = Vector3(vector_in.x, 0, vector_in.z)
	return vector_out.length()
