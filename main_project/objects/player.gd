extends CharacterBody3D

const jump_sound = [
		"res://resources/sounds/Catgirl Fighting SFX/jump/001_jump.wav",
		"res://resources/sounds/Catgirl Fighting SFX/jump/002_jump.wav",
		"res://resources/sounds/Catgirl Fighting SFX/jump/003_jump.wav",
		"res://resources/sounds/Catgirl Fighting SFX/jump/004_jump.wav",
		"res://resources/sounds/Catgirl Fighting SFX/jump/005_jump.wav",
		"res://resources/sounds/Catgirl Fighting SFX/jump/006_jump.wav",
		"res://resources/sounds/Catgirl Fighting SFX/jump/007_jump.wav"
		]

var next_jump_sound = 0

var jump_velocity = 12
var jump_boost = 2.0
var fall_threshold = -100.0
var basic_friction = 15.0
var air_speed = 30.0
var walk_speed = 30.0
var run_speed = 18.0

var max_air = 3.0
var max_walk = 4.0
var max_run = 12.0

var grab_limit = 2.0

var mouse_x_sensitivity = 1.0 / 50000.0
var mouse_y_sensitivity = 1.0 / 50000.0

var min_velocity = .001

var paused = true
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")

var wall_run_force = gravity * .9

var grab_ledge_force = gravity * 1.1

var standing_on = ""

var the_floor

var touching_grab_ledge = false

func _ready():
	randomize()

func _physics_process(delta):
	if not paused:
		#Not movement related
		out_of_bounds_check()
		look()
		
		#affect velocity
		apply_gravity(delta)
		walk_or_run(delta)
		wall_run(delta)
		grab_ledge(delta)
		apply_friction(delta)
		jump(delta)
		
		#perform movement
		move_and_slide()

func look():
	var mouse_move = Input.get_last_mouse_velocity()
	var cam = $Camera3D
	rotate_y(mouse_move.x * mouse_x_sensitivity * -1)
	cam.rotate_x(mouse_move.y * mouse_y_sensitivity * -1)
	cam.rotation.x = clamp(cam.rotation.x, -PI/2, PI/2)

func walk_or_run(delta):
	var input_dir = Input.get_vector("left_strafe","right_strafe","forward","backward")
	var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	
	if not direction.length() > 0:
		return #no input
	
	var new_velocity = velocity
	
	#debug
	var h_velocity = h_speed(velocity)
	get_tree().call_group("debug_handler", "update_hvel", h_velocity)

	if not is_on_floor():
		#in the air
		new_velocity = velocity + (direction * air_speed * delta)
		if not going_too_fast(new_velocity, velocity, max_air):
			velocity = new_velocity
			return
	else:
		#on the ground
		new_velocity = velocity + direction * walk_speed * delta
		if not going_too_fast(new_velocity, velocity, max_walk):
			velocity = new_velocity
			return
		new_velocity = velocity + direction * run_speed * delta
		if not going_too_fast(new_velocity, velocity, max_run):
			velocity = new_velocity
			return

func jump(delta):
	if Input.is_action_just_pressed("jump") and is_on_floor() and $FrameTimer.is_stopped():
		#Start Jump
		$FrameTimer.start()
		velocity.y += jump_boost
		play_jump_sound()
	if Input.is_action_pressed("jump") and not $FrameTimer.is_stopped():	
		velocity.y += jump_velocity * delta

func apply_gravity(delta):
	var low_gravity = gravity / 2.0
	if is_on_floor():
		velocity.y -= low_gravity * delta
	else:
		velocity.y -= gravity * delta
		
func apply_friction(delta):
	#IN_PROGRESS!!
	
	# Not touching floor
	if not is_on_floor():
		return
		
	# Moving
	if velocity.length() == 0:
		return
		
	var floor_friction = get_floor_friction() * delta
	
	var friction_vector = Vector2(velocity.x, velocity.z)
	
	friction_vector = friction_vector.move_toward(Vector2.ZERO, floor_friction)
	
	velocity.x = friction_vector.x
	velocity.z = friction_vector.y

func out_of_bounds_check():
	if position.y < fall_threshold:
		print_debug("out of bounds")
		get_tree().quit()

func get_floor_friction_old():
	var friction = basic_friction
	
	var collision = get_last_slide_collision()
	if not collision is KinematicCollision3D:
		return friction
	
	var collider = collision.get_collider()
	if collider is Node:
		standing_on = collider.name
	if not collider is StaticBody3D:
		return friction
	
	var material = collider.physics_material_override
	if not material is PhysicsMaterial:
		return friction
	
	#finally
	friction = friction * material.friction * 2
	return friction

func get_floor_friction():
	var friction = basic_friction
	
	#Get floor name for debug
	if the_floor is Node:
		standing_on = the_floor.name
	
	#Check that floor is correct type
	if not the_floor is StaticBody3D:
		return friction
	
	var material = the_floor.physics_material_override
	
	#check for null physics material
	if not material is PhysicsMaterial:
		return friction
	
	#finally
	friction = friction * material.friction * 2
	return friction
	
func _on_feet_body_entered(body):
	the_floor = body

func h_speed(vector_in):
	var vector_out = Vector3(vector_in.x, 0, vector_in.z)
	return vector_out.length()

func going_too_fast(vector_new, vector_old, maximum):
	var going_faster = h_speed(vector_new) > h_speed(vector_old)
	var going_really_fast = h_speed(vector_new) > maximum
	if going_faster and going_really_fast:
		return true
	else:
		return false	

func jump_time():
	return $FrameTimer.frames_left

func wall_run(delta):
	if is_on_floor():
		return
	if velocity.y > min_velocity:
		return
	if not Input.is_action_pressed("jump"):
		return
	if not touching_wallrun():
		return
	velocity.y += wall_run_force * delta
	
func touching_wallrun():
	var sides = $Sides
	return sides.has_overlapping_bodies()

func grab_ledge(delta):
	var velocity_reduction = 0.8
	if not touching_grab_ledge:
		return
	if velocity.y > grab_limit:
		return
	if Input.is_action_pressed("jump"):
		if velocity.y < 0:
			velocity.y = velocity.y * velocity_reduction
		velocity.y += grab_ledge_force * delta

func touching_ledge(value):
	touching_grab_ledge = value

func play_jump_sound():
	var asp = $AudioStreamPlayer_jump
	if(not asp.playing):
		var n = randi() % jump_sound.size()
		var sound = load(jump_sound[n])
		asp.stream = sound
		asp.play()
		
