extends StaticBody3D

const EnvironmentLayer = 2
const PlayerLayer = 3

var prime_origin : Vector3
var ready_to_fall = false
var sink_speed = 0.5
var fall_speed = 5.0

# Called when the node enters the scene tree for the first time.
func _ready():
	prime_origin = transform.origin

func _physics_process(delta):
	move_and_collide(constant_linear_velocity * delta)

func _on_fall_detector_body_entered(body):
	var time = $SlowFallTimer
	if not ready_to_fall:
		ready_to_fall = true
		time.start()

func _on_slow_fall_timer_timeout():
	make_block_fall()

func make_block_fall():
	var time = $FastFallTimer
	time.start()
	constant_linear_velocity = Vector3.DOWN * sink_speed

func _on_fast_fall_timer_timeout():
	var time = $DespawnTimer
	time.start()
	constant_linear_velocity = Vector3.DOWN * fall_speed


func _on_despawn_timer_timeout():
	var time = $RespawnTimer
	time.start()
	despawn()

func despawn():
	constant_linear_velocity = Vector3.ZERO
	hide()
	disable_collision()

func disable_collision():
	set_collision_layer_value(EnvironmentLayer, false)
	set_collision_mask_value(PlayerLayer, false)

func enable_collision():
	set_collision_layer_value(EnvironmentLayer, true)
	set_collision_mask_value(PlayerLayer, true)


func _on_respawn_timer_timeout():
	respawn()
	
func respawn():
	transform.origin = prime_origin
	show()
	enable_collision()
	ready_to_fall = false
