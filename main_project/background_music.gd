extends AudioStreamPlayer


# Called when the node enters the scene tree for the first time.
func _ready():
	stream = load("res://resources/sounds/LDD - far beyond - Chillhop Music Vol. 01 [Free Pack]/far beyond - OGG FILES/LDD_CHFB_01_to someone.ogg")
	volume_db = -20.0
	finished.connect(_playback_ends)
	play()
	
func _playback_ends():
	play()
