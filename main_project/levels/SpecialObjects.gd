extends Node3D

var bonus_time = 0
@onready var GUI = $"../Main_GUI"

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	update_timer_text()
#	update_bonus_time()


func _on_start_gate_body_entered(body):
	if body.name != "Player":
		return
	if not $MainLevelTimer.is_stopped():
		return
	$MainLevelTimer.start()
	$OrbSound.stream = load("res://resources/sounds/Big Egg collect 1.wav")
	$OrbSound.volume_db = 2.0
	$OrbSound.play()
	$StartGate.queue_free()
#	$DebugTimer.start()

func update_timer_text():
	if $MainLevelTimer.is_stopped():
		$TimerDisplay.hide()
	else:
		var current_time = $MainLevelTimer.time_left
		$TimerDisplay.text = str(current_time).pad_decimals(1)
		$TimerDisplay.show()

func time_orb_collected():
	var timer = $MainLevelTimer
	if not timer.is_stopped():
		timer.start(timer.time_left + 1.0)
		$OrbSound.volume_db = -15.0
		$OrbSound.playSound()

#func update_bonus_time():
#	bonus_time = $MainLevelTimer.time_left - $DebugTimer.time_left


func _on_goal_fish_body_entered(body):
	finish_level()
	
func finish_level():
	$MainLevelTimer.paused = true
	GUI.finish_screen()
