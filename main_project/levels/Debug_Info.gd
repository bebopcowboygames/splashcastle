extends Label

var my_player
var my_special_objects
var my_side_area
var debug_text = "debug"

var h_vel = 0.0

func _ready():
	my_player = get_player()
	my_side_area = my_player.find_child("Sides")
	my_special_objects = get_special_objects()
#	list_all_owners()

func _process(delta):
	if(Input.is_action_just_pressed("debug")):
		show_hide()
	if(visible):
		update_debug()

func show_hide():
	visible = not visible

func update_debug():
	debug_text = "Debug:"
	
#	add_debug("Velocity: " + player_velocity())
#	add_debug("Standing on: " + standing_on_what())
#	add_debug("Bonus time: " + my_bonus_time())
#	add_debug("Velocity: " + hvel_value())
#	add_debug("Jump_timer: " + get_jump_timer())
	add_debug("Wall-Run?: " + wall_run_check())
	
	text = debug_text
	
func get_player():
	return get_parent().find_child("Player")

func player_velocity():
	var pv = my_player.velocity.length()
	return str(pv).pad_decimals(2)

func add_debug(input_text):
	var new_line = "\n" + input_text
	debug_text = debug_text + new_line

func standing_on_what():
	return my_player.standing_on

func get_special_objects():
	return get_parent().find_child("SpecialObjects")

func my_bonus_time():
	return str(my_special_objects.bonus_time).pad_decimals(2)

func update_hvel(new_hvel):
	var h_vel = new_hvel

func hvel_value():
	return str(h_vel).pad_decimals(2)
	
func list_all_owners():
	var p = get_parent()
	var children = recurse_children(p)
	for nodes in children:
		print_debug(nodes.name + " : " + nodes.owner.name + "\n")

func recurse_children(node):
	if node == null:
		return Array()
	var my_children = node.get_children()
	var other_children = my_children
	for child in my_children:
		other_children.append_array(recurse_children(child))
	return other_children

func get_jump_timer():
	var time = my_player.jump_time()
	var time_string = str(time).pad_decimals(0)
	return time_string

func wall_run_check():
	var check = my_side_area.has_overlapping_bodies()
	return str(check)
