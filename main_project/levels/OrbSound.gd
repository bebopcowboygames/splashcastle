extends AudioStreamPlayer

const fencing_hit_sound_prefix = "res://resources/sounds/FilmCow Recorded SFX/fencing hit "
const wav_suffix = ".wav"
const total_sounds = 17

func playSound():
	stop()
	var sound = load(randomFileName())
	stream = sound
	play()

func randomFileName():
	var number = (randi() % total_sounds) + 1
	return fencing_hit_sound_prefix + str(number) + wav_suffix
