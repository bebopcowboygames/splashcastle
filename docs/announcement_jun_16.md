# Major Redesign Announcement

Okay, so I'm sort of starting from scratch on this project again. I hope that my experience with working on this will help me to rebuild the project quicker and cleaner. I kept all my old code and assets for reference of course, but I've put them in a separate section so I only see them if I need to. I'm also changing the scope of my game pretty drastically. I'm no longer working on a two-part game like before. I've decided to ditch the underwater free-movement sections entirely for now. You can see a new description of the game on the main README doc now, but I'm focusing just on making a first person platformer now.  I also have a much simpler mental image of the game overall.

## Four Week Challenge

This time I am giving myself a hard deadline. I believe that I can make a simple game within the scope of four weeks if I focus and work hard. So, this is my new goal, 4 weeks. I expect to have this game finished by July 15th. It's not going to be super complicated. I'm not even sure that I can actually charge for it, but my goal is to have it done and published to itch.io by July 15th. 

I'm also going to try and give regular social media updates on my progress again, but if the social media posts fall behind I'm not going to worry as long as actual progress is being made on the game.

## Starter Game

Even though I've stripped back the goals of this game to something much simpler, I'm hoping that I can use this game as a sort of template for future games. Use my work on this to make bigger and better gaames later. I need to worry less about this game having everything I want and more about it being a playable, tangible thing.

## New Design Summary

Keep up with the game design on the main README here:

[README](https://gitlab.com/bebopcowboygames/splashcastle/)
