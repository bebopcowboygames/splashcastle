# Design Document Detail
## Splash Castle
---
## Overview

A first-person platformer made with the Godot game engine with a focus on creative movement especially featuring slippery surfaces. This simple game will form the foundation for more complicated titles in the future. The code for the game will focus on simplicity and readability so that it can be easily reused by myself and others.

This game will be seperated into 10 levels. At the start of each level, the player will be given 30 seconds to reach the level's goal. Across the level will be icons to collect that will add an additional second onto the timer. Each level will have 60 of these bringing the total to 90 seconds per level or 15 minutes for the whole game.

The game will have a medieval fantasy theme with a bright, happy mood. The castle structure will be mostly flooded with flowing water along stone walls and large pools of water. Most levels will focus on climbing vertically to a goal at the highest possible point. 

## Features

 + player character
    - move
    - jump
    - friction
    - wall run
    - wall jump
 + terrain
    - sliding slopes
    - grab ledges
    - elevator lifts
    - water
    - walls
    - floor
    - platforms
    - crumble platforms
    - slide ramps
 + time gems
 + goal chalice


## Level Design

Each level should feature a new mechanic or new type of trick using an existing mechanic slowly introducing the character to the game.

### Level 1 - Intro

All levels will begin with a Lobby area where there is no timer or pick-ups.  This area is just to get the player adjusted to moving before beginning. The first level will have a larger than usual lobby area to teach the player how to move around the game. Each level when then have a start gate that will start the timer for that level.

Level 1 will be just simple running and jumping with easy platforming to get the player acquainted with the goal of the game.

### Level 2 - Crumble Blocks

### Level 3 - Water

### Level 4 - Slippery slopes

### Level 5 - Wall Running

### Level 6 - Grab ledges

### Level 7 - Wall jumps lateral

### Level 8 - Wall jumps backward

### Level 9 - Slide Ramps

### Level 10 - Finale
