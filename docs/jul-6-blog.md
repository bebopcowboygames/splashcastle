# Special Log

I'm pushing back my goal date. There have been a few unexpected time sinks in my personal life during the project. Lately I've been very productive, but a couple weeks ago I was occupied taking care of personal business. Despite moving the date back, I'm feeling really good about the progress of the project this time. I don't know if the project management has helped or if I'm just starting to get the hang of it with practice.

The new goal date is August 4th. I will be taking a trip the week of Jul 17-21 so I expect to get little to no work done that week. That gives me about 3 more weeks worth of work. 

Now let me tell you what I'm thinking about long-term. Once I finish this game, I'm going to post it on itch.io for free just to get my itch developer account going. Then I think I'm going to focus on the Flask class I was taking and see if I can't finish that off. I don't know exactly how long that class will take, it shouldn't be too much, but I also underestimated the Godot course I took before. Glad I did take it, that course really taught me everything I needed for Godot. After Flask class is over or maybe during the class, I'm going to work on creating a professional webpage again. I want to make it from scratch, html, css, and only my own personal js. I want it to be the leanest meanest webpage I can make that way I can impress people with how quickly it loads. Going to try and put any webapps I develop on the same server, probably a DigitalOcean droplet.

Then even further down the road I'll start another game. I want to develop a list of game ideas and get some feedback on which idea people think I should work on next. If you read all of this then a big thank you! I'm really just writing this stuff for my own reflection, but I so happy you care enough to read all this. <3

