## Tue, Aug  1 2023

I'm back. After my big vacation trip things have been slowly getting back to where it was before. I think I'm going to need to move my goal date back again, but I'm going to wait until after I've worked all week to try and get a better idea of how long I need to push it back.

My current mission: get one good working level finished.  I have plans for 10 levels, but I want to get the first one finished to get an idea of how long the rest of the project might take.

## Tue, Jul 11 2023

Today I spent a good bit of time learning something new. I wanted to have a sort of pixelated look to my art for this game (and probably future games), but the settings I used to use in Godot 3.5 "pixel presets" were no longer there. Instead  they moved the texture interopolation (spelling?) to the project settings instead. It says its just the "default" interopolation setting, but I can't find anywhere to manually change it per image. It doesn't matter though, I would change it for everything anyway.

## Mon, Jul 10 2023

Missed days:
 * Friday - I worked on the project but didn't have time to log.

I barely touched Godot today. I spent most of my time planning things on my kanban board. I also wrote out some level design ideas on the design doc. I think tomorrow I'm going to try and work on some of the artwork.

## Thu, Jul 6 2023

[Special Log](jul-6-blog.md)

## Wed, Jul 5 2023

Missed days:
 * Friday - ???
 * Monday - Counseling appt
 * Tuesday - 4th of July

Worked on the GUI today. I want to be able to easily switch to fullscreen mode and back while working on the game so I thought I'd get the basics of this handled. 15th of July deadline is looking to be to close for realistic expectations. I think I'm going to move the goal forward to August 4th.


## Thu, Jun 29 2023

No post for two days? Yikes. Let's see what happened:
 * Tuesday - Had bible study in the morning and then got lost in Element messenger stuff in the afternoon.
 * Wednesday - cleaned my apartment all morning, slept for like 2 hours in the afternoon.

So, not really good excuses, but I'm back today. I like Taiga, really helps me to plan things out. It's a little annoying having to redo all this code again, but that's what I get for starting over.  Really hope this time it comes out cleaner and simpler. It really gets overwhelming when all the stuff starts to tangle up together.

I think that in order to be a better solo game developer I'm going to have to become better at art. Like I don't need to be an 'Artist' per se, but I have to be a little bit of an artist just to make it work.

## Mon, Jun 26 2023

Time to hit the road running. First off I'd like to introduce my new project management tool:
[Taiga - Splash Castle Project](https://tree.taiga.io/project/thebebopcowboy-splash-castle/kanban)

Every time I add a new feature I'm going to put it on this Kanban board. I think I can use this to help me plan out my work and look at the project on a bigger scale. This might wind up replacing both my 'Next Steps' doc and maybe my 'Design doc'. But maybe not. I'd like to be able to export a record of what I do on my Taiga project to a file, but I'm not sure whether that is a feature or not.

## Thu, Jun 22 2023

Okay, not a great start skipping 3 days of the week, but I had stuff going on:
 * Monday was my counseling appointment and other things
 * Tuesday I help mom take the dog to get groomed and hung out with Brian
 * Wednesday I went with Bekah to her appointment and hung out with Mike

Anyway, I think I can accept my lack of productivity this week and still push things along. I hope that I can get done in my 4 week time frame, but worst case scenario I have to throw another month on. I think after next week I'll have a better idea of how feasible this time frame is.

## Fri, Jun 16 2023

I'm sort of starting over with this project. I'm going to write a detailed explanation [here](announcement_jun_16.md).

