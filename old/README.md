# Splash Castle
## (Working Title)

This project is going to be an FPS game made with the Godot game engine. In this game, the player will explore ancient flooded ruins. The game will be divided into two forms of gameplay; above water platformer and below water free moving shooter. 

Here on my Gitlab repo you can browse my code in progress. You might want to start with some of my documentation:

## [Design Summary](/docs/design_summary.md)

This is a summary of the game's design, it describes what the game will be like in more detail.

## [Work Log](/docs/work_log.md)

This is a daily journal of progress on the project.
