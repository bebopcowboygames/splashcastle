extends Control

func _input(event):
	if event is InputEventKey:
		start_pressed()
		pause_pressed()
		quit_pressed()

func start_pressed():
	var player = get_parent().find_child("Player")
	if not Input.is_action_just_pressed("Start"):
		return
	DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	player.paused = false

func pause_pressed():
	var player = get_parent().find_child("Player")
	if not Input.is_action_just_pressed("Pause"):
		return
	mouse_mode_toggle()
	screen_mode_toggle()
	player.paused = not player.paused

func mouse_mode_toggle():
	if Input.mouse_mode == Input.MOUSE_MODE_VISIBLE:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	else:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE

func screen_mode_toggle():
	if DisplayServer.window_get_mode() == DisplayServer.WINDOW_MODE_WINDOWED:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
	else:
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)

func quit_pressed():
	if not Input.is_action_just_pressed("ui_cancel"):
		return
	get_tree().quit()
