extends CharacterBody3D

const MOVE_SPEED = 20
const JUMP_STRENGTH = 300
const STOP_THRESHOLD = .02
const MOVING_THRESHOLD = .01
const CAM_SENSITIVITY_X = -1.0/20000
const CAM_SENSITIVITY_Y = -1.0/20000
const FRICTION_ADJUSTER = 10
 
# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var paused = true

# debug values - remove in final
var collider_debug = ""
var friction_debug = 0.0

func _physics_process(delta):
	
	var add_velocity = Vector3.ZERO
	
	add_velocity += get_gravity(delta)
	add_velocity += get_walk_motion(delta)
	add_velocity += get_jump_motion(delta)
	add_velocity += get_friction(delta, speed())
	
	velocity += add_velocity
	
	# Slope slide fix
	if speed() < STOP_THRESHOLD:
		velocity = Vector3.ZERO
	
	move_and_slide()
	turn_camera()

func get_gravity(delta):
	var gravity_vector = Vector3.DOWN * gravity * delta
	
	if(is_on_floor()):
		return Vector3.ZERO
	else:
		return gravity_vector

func get_walk_motion(delta):
	
	if paused:
		return Vector3.ZERO
		
	if (is_on_floor()):
		# Get the input direction and handle the movement/deceleration.
		var input_dir = Input.get_vector("Left", "Right","Forward","Backward")
		var direction = (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
		return direction * MOVE_SPEED * delta
	else: 
		return Vector3.ZERO

func get_jump_motion(delta):
	if paused:
		return Vector3.ZERO
	
	if (is_on_floor() and Input.is_action_pressed("Jump")):
		return JUMP_STRENGTH * delta * Vector3.UP
	else:
		return Vector3.ZERO

func get_friction(delta, speed):
	var moving = speed > MOVING_THRESHOLD
	var friction_coeff = 0.0
	if (is_on_floor() and moving):
		friction_coeff = tweak_friction(get_floor_friction())
		var friction_vec = velocity * delta * friction_coeff
		return friction_vec
	else: 
		return Vector3.ZERO

func turn_camera():
	if paused:
		return
	var my_cam = $Camera3D
	var mouse_motion = Input.get_last_mouse_velocity()
	rotate_y(mouse_motion.x * CAM_SENSITIVITY_X)
	my_cam.rotate_x(mouse_motion.y * CAM_SENSITIVITY_Y)
	my_cam.rotation_degrees.x = clamp(my_cam.rotation_degrees.x, -80, 80)

func speed():
	return velocity.length()

func get_floor_friction():
	const DEFAULT_FRICTION = 0.5
	
	var last_slide:KinematicCollision3D
	var floor_body:StaticBody3D
	var floor_material:PhysicsMaterial
	
	last_slide = get_last_slide_collision()
	if not last_slide is KinematicCollision3D:
		return DEFAULT_FRICTION
	
	floor_body = last_slide.get_collider()
	if not floor_body is StaticBody3D:
		return DEFAULT_FRICTION
	
	floor_material = floor_body.physics_material_override
	if not floor_material is PhysicsMaterial:
		return DEFAULT_FRICTION
	
	collider_debug = floor_body.get_class()
	
	friction_debug = floor_material.friction
	
	return floor_material.friction
	# get collider and send name to debug

func tweak_friction(original):
	return original * -1 * FRICTION_ADJUSTER
