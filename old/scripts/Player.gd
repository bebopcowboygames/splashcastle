extends KinematicBody

const MAX_SLIDES = 4
const FLOOR_MAX_ANGLE = deg2rad(90 * (4/5))
const STOP_ON_SLOPE = false
const INIFINITE_INERTIA = true

export var move_speed = 50
export var jump_strength = 50
export var buffer_frames_max = 3

var prev_velocity = Vector3.ZERO
var on_floor = false
var on_wall = false
var floor_buffer_frames = 0
var wall_buffer_frames = 0

func _physics_process(delta):
	var velocity = prev_velocity
	on_floor = on_floor_buffered()
	on_wall = on_wall_buffered()
	
	velocity += walk_velocity()
	velocity += jump_velocity()
	velocity += friction_velocity()
	prev_velocity = move_and_slide(velocity, Vector3.UP, STOP_ON_SLOPE, MAX_SLIDES, FLOOR_MAX_ANGLE, INIFINITE_INERTIA)

func _input(event):
	pass

func walk_velocity():
	var walk_vector = Vector3.ZERO
	
	walk_vector = new_walk_velocity()
	
	return walk_vector

func new_walk_velocity():
	var new_walk = Vector3.ZERO
	if Input.is_action_pressed("forward"):
		new_walk += Vector3.FORWARD
	if Input.is_action_pressed("backward"):
		new_walk += Vector3.BACK
	if Input.is_action_pressed("left"):
		new_walk += Vector3.LEFT
	if Input.is_action_pressed("right"):
		new_walk += Vector3.RIGHT
	
	new_walk = new_walk.normalized()
	new_walk = rotate_to_match(new_walk)
	new_walk = new_walk * move_speed
	return new_walk

func can_walk():
	var walk_value = false
	return walk_value

func jump_velocity():
	if Input.is_action_pressed("fire_jump"):
		return jump_strength
	else: return Vector3.ZERO

func friction_velocity():
	pass

func rotate_to_match(vector_in):
	var vector_out = vector_in.rotated(Vector3.RIGHT,rotation.x)
	vector_out = vector_out.rotated(Vector3.UP, rotation.y)
	vector_out = vector_out.rotated(Vector3.BACK, rotation.z)
	return vector_out

func on_floor_buffered():
	if is_on_floor():
		floor_buffer_frames = buffer_frames_max
		return true
	elif floor_buffer_frames > 0:
		floor_buffer_frames -= 1
		return true
	else:
		return false

func on_wall_buffered():
	if is_on_wall():
		wall_buffer_frames = buffer_frames_max
		return true
	elif wall_buffer_frames > 0:
		wall_buffer_frames -= 1
		return true
	else:
		return false
