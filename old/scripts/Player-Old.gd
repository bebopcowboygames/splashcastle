extends KinematicBody

var velocity = Vector3.ZERO
var acceleration = Vector3.ZERO
var near_wall = false
var wall_friction

# var h_move_speed = H_MOVE_SPEED_MIN

const VPA = 10.0 #velocity proportion tweaking
const H_MOVE_SPEED_MIN = 5
const H_MOVE_SPEED_MAX = H_MOVE_SPEED_MIN * 10
const GRAVITY = Vector3.DOWN * 9.8
const TURN_SPEED = 1.0 / 1000
const ANGLE_SPEED = 1.0 / 1000
const JUMP_STRENGTH = Vector3.UP * 1000
const AIR_RESISTANCE = 0.01
const SNAP_THRESH = 0.5
const WALL_FACTOR = 0.5
# const SNAP_VECTOR_DEFAULT = Vector3.DOWN * 2.2

func _physics_process(delta):
	
	var fric_coeff = AIR_RESISTANCE
	var on_floor = false
	acceleration = GRAVITY
	
	if is_on_floor():
		on_floor = true
		fric_coeff = get_slide_friction(fric_coeff)
		acceleration += walk()
		acceleration += jump()
		acceleration = rotate_to_match(acceleration)
	
	# Horizontal friction	
	acceleration += friction(fric_coeff)
	
	# if on wall then apply wall run friction
	# also test for wall jump
	# (?) also check for ledge
	if is_near_wall() and not on_floor:
		fric_coeff = wall_friction
		acceleration += wall_run(fric_coeff)
	
	velocity += acceleration * delta
	
	var stop_on_slope = (velocity.length() < SNAP_THRESH) and on_floor
	
	if stop_on_slope:
		velocity.y = 0
	else:
		velocity = move_and_slide(velocity, Vector3.UP, stop_on_slope, 4, deg2rad(60))
	

func _input(event):
	## Menu Actions
	if Input.is_action_just_pressed("capture_mouse"):
		toggle_mouse_mode()
	if Input.is_action_just_pressed("ui_cancel"):
		get_tree().quit()
	
	# mouse input
	if event is InputEventMouseMotion:
		rotate(Vector3.UP, event.relative.x * -TURN_SPEED)
		$Camera.rotate_x(event.relative.y * -ANGLE_SPEED)
		$Camera.rotation.x = clamp($Camera.rotation.x, -PI/4, PI/4)


func walk():
	var walk_vector = Vector3.ZERO
	if Input.is_action_pressed("forward"):
		walk_vector += Vector3.FORWARD * H_MOVE_SPEED_MAX
	if Input.is_action_pressed("backward"):
		walk_vector += Vector3.BACK * H_MOVE_SPEED_MAX
	if Input.is_action_pressed("left"):
		walk_vector += Vector3.LEFT * H_MOVE_SPEED_MAX
	if Input.is_action_pressed("right"):
		walk_vector += Vector3.RIGHT * H_MOVE_SPEED_MAX
	return walk_vector

func jump():
	if Input.is_action_pressed("fire_jump"):
		return JUMP_STRENGTH
	else: return Vector3.ZERO 

func friction(coeff):
	var f_vec = velocity * VPA
	f_vec += acceleration
	f_vec = f_vec * -coeff
	return f_vec

func rotate_to_match(vector_in:Vector3):
	var vector_out:Vector3 = vector_in.rotated(Vector3.RIGHT,rotation.x)
	vector_out = vector_out.rotated(Vector3.UP, rotation.y)
	vector_out = vector_out.rotated(Vector3.BACK, rotation.z)
	return vector_out

func toggle_mouse_mode():
	if Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
	else:
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func wall_run(co_eff):
	var push_vec = Vector3.UP * co_eff * GRAVITY.length() * WALL_FACTOR
	return push_vec

func get_slide_friction(fric):
	var new_fric = fric
	if get_last_slide_collision() is KinematicCollision:
			var this_surface = get_last_slide_collision().collider
			if this_surface is StaticBody:
				if this_surface.physics_material_override is PhysicsMaterial:
					new_fric = this_surface.physics_material_override.friction
	return new_fric

func is_near_wall():
	if near_wall:
		if $WallBuffer.get_overlapping_bodies().empty():
			near_wall = false
	elif is_on_wall():
		wall_friction = get_slide_friction(AIR_RESISTANCE)
		near_wall = true
	return near_wall
