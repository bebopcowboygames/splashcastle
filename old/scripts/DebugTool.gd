extends Node

onready var player_node = get_parent().get_node("Player")

func _process(delta):
	button_break()
	debug_text()

func button_break():
	if Input.is_action_just_pressed("debug_key"):
		pass #break

func debug_text():
	var message = "Debug Menu\n"
	message += on_wall_test()
	$DebugText.text = message

func on_wall_test():
	return "Near wall: " + str(player_node.is_near_wall()) + "\n"
