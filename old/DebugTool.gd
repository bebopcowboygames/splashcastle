extends Control

@onready var player = get_parent().find_child("Player")

func _process(delta):
	update_label()

func update_label():
	var label = $Label
	var speed_str = "Speed: " + str(snappedf(player.speed(), .001))
	var collide_str = "\nCollider: " + player.collider_debug
	var fric_str = "\nFriction = " + str(snapped(player.friction_debug, .001))
	label.text = speed_str + collide_str + fric_str
