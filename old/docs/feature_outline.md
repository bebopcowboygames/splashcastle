# Splash Castle Design Doc

# Above Water

## Player

 - Basic movement and gravity
 - Slope Physics
 + Slope jumps
   + Add angular jump force
 + Slippery Physics (Friction)
 + Wall running
 + Wall jumping
 + Ledge grabbing
 + Floating/Swimming

## Platforms
 + Bounce pads
 + Elevators
 + Climb walls
 + Grab ledges
 + Slippery slopes (ha) 

## Powerups
 + 12 (orbs?) per level to unlock door
 + 3 secret rewards per level (gold orbs?) for collection
 + (grappling hook?)


# Below Water

## Enemies

 + Floating Mines
 + Octopus Grabbers
	* Attempt to grab and pull you towards them
 + Basic Fish baddies
	* Follow and shoot you

# Controls

above water / below water

## Controller

 + Left stick: AIM up, down, left, right
 + Right stick: MOVE forward, backward, left, right (strafing)
 + L/R buttons: TBD / ROLL left, right
 + L/R triggers: Dash, Jump / Block, Fire
 + Button 3 (X): TBD / Ascend
 + Button 4 (Y): TBD / Descend

## Mouse/Keyboard

 + Mouse: AIM pitch and yaw
 + WASD: MOVE (strafing)
 + Q/E: ROLL left, right
 + Mouse 1: Main fire
 + Mouse 2: Alt fire
 + R/F: Ascend, Descend
