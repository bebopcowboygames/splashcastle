# Work Log 2023

## Jan 13
Mostly worked on documentation today. Trying to get ideas on paper. I've been working on setting up this Git repo and doing some basic project setup for Godot over the last few days. I'm excited to get started on this.

## Jan 23
I haven't been very good about keeping this log up to date. I also haven't been using my time tracker like I need to. Going to do better this week. I'd like to put down a few TODO items:

 * Add jump
 * Work on strange acceleration after standing still
 * Add camera tilt to mouse aim

## Jan 24
I've still been having trouble working in the mornings. I don't think it's a big deal because I am more focused later in the day anyway, but I think I could get more done if I was able to work a little more in the mornings.
The physics is still a little buggy and I need to figure it out because the more I add to the game the harder it will be to figure out what's causing it. Otherwise, adding features seems to be going smoothly enough. My character has:
 * gravity
 * friction
 * mouse aim
 * jumping
Next time I need to work on platforms so that I can have a more detailed environment to test in. I need to add:

 * cube platforms
 * basic ramps

## Jan 25
I've been putting in a lot more hours this week. I feel like I might be getting into a habit. I think I fixed the jankiness in my physics. It was basically that I had some of my physics in the \_input method instead of the \_physics method.

Working on this bug, I didn't get much time to work on the level so next time I need to work on these:

 * basic ramp
 * tile grid (maybe)
 * big wall
 * big floor
 * big roof (maybe)
 * think about lights

## Jan 26
Spent a good two hours today just picking a palette for the game. I felt like doing something art related. I think doing the art for the game is going to be more time-consuming than I'd like. I think I'm going to try to limit myself to my 16-color palette. I think that will help give my game more of a theme. I made a very blue palette cause underwater y'know? I wonder if I should do a second palette, above water underwater sort of thing. I'll work on that later, I need to do more code.

## Jan 31
Had a shift in thinking today. I've been trying to get some basic art to give my concepts some shape, but its been a little trying for me. I've decided to work with more abstract graphics at the moment and focus on code, it's my strong suit after all. After the game mechanics are a bit more solid I may come back to the graphics.

## Feb 8
Ok, it's been a while since I wrote in the log.  Also I haven't been doing too much work since then.  I had a bug with the physics and the friction that took a while to fix.  Also, I've just been having a little trouble with motivation.  I'm back on it now though. Fixed the friction problem, walking moving smooth.  I plan on working on wall jumps next.
  I think that getting the physics just right before moving on is going to be important.  All code builds on previous code, if there's something that's not quite right with the basic stuff then that issue is going to multiply as I add more levels of complexity on top of it.
  After a couple of weeks, I want to have a working physics demo for the platforming section.  It won't look pretty but it should move smoothly. I want to implement:
 * Wall running
 * Wall jumping
 * Ledge grabbing / grippable ledges
 * Water bouyancy
 * climbable walls
 * bounce pads

## Feb 20

AHHHH! Ok, I have been majorly slacking lately in both writing these posts and working on the project in general. But I am back! I took a step away to work on server admin, Matrix chat, and other stuff. Also I've just been less productive but I'm going now.

I set up some basic wall-running physics and the friction is all good now. I also created an on-screen debug menu to help with figuring out problems on the fly. I added a near-wall function to the player to accomodate the wall-running, it might be useful elsewhere as well. Also I finally threw in strafing, four lines of simple code, boy do I procrastinate sometimes.

I guess the next task at hand is:
 * Wall Jumping

## Mar 21

Well, I'm rebooting the project.  I've been stuck re-writing things and working on other personal life a lot lately, but I'm back on Git again. Things are going back on record and progress will be made!

Godot 4 came out recently and I've restarted the project in that engine. I was thinking about restarting before version 4 was announced, but now just seems like a good time. I've moved all the old project files into an "old" folder.

I struggled with whether starting over was a good idea or not, but I'm starting to think now that this is just my process. I iterate work. I do something until it becomes messy then I sweep it all aside, start over, and even though I repeat things I do it better and faster the next time around. I just iterate until I can do it flawlessly, or like, at least pretty good.

Most of my time before was spent working on the character physics script. Now that I have a better grasp of what Godot can do with that I think I can write cleaner code based on my older code.

Ok, I've got the player physics going. I needed to relearn some of the new ways that Godot 4 handles physics.  I used their template script and looked up every line I didn't understand. They no longer have arguments for 'move and slide'. Velocity is an object variable. They have a new method for finding input vectors, and they have a transform option that optimizes 3d rotation and I still don't fully understand that one yet.

# Apr 11

    OKAY! Yes, I have been slacking on this project. It's time to get back into action. Tracking my time, documenting my work, posting online, all the good stuff.  I have moved my project entirely over to Godot 4.0 which did cause me to have to re-do a lot of stuff, but I think it was worth it. I think iterating through work helps me to do better work. I have to do things over and over again, but every time I get better and faster at it. That's how I've always heard athletes do it. They do the same basic things over and over until it becomes second nature and that helps them to pull of the bigger feats. I think the same thing might apply to code.

    I've also rearranged my documentation structure. I have separate design doc, feature list, to-do list, and work log. I specifically hope this separate to-do list will help to keep me moving. Looking at the whole feature list can be too daunting, but useful for a big picture analysis. Putting the to-do list in here was alright, but a little awkward.

# Apr 12

    Okay, got my little debugger re-coded in 4.0. I think I've got the friction to a stable state too. I think I might adjust things later so that on "rough" materials you can go faster but stop quicker or something, but it's fine for now. I've really been getting back in the groove lately which feels great. I just hope I can keep it up longer than I have before.












END
