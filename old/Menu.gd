extends Panel

enum {PAUSE_MODE, PLAY_MODE}

var game_mode = PAUSE_MODE
var player_object: CharacterBody3D

func _ready():
	player_object = get_parent().find_child("PlayerFirstPerson")

func _process(delta):
	if Input.is_action_just_pressed("ui_cancel"):
		quit_pressed()
	if Input.is_action_just_pressed("Pause"):
		toggle_pause()

func pause_pressed():
	toggle_pause()

func quit_pressed():
	get_tree().quit()
	
func toggle_pause():
	if game_mode == PAUSE_MODE:
		game_mode = PLAY_MODE
		player_object.game_mode = player_object.PLAY_MODE
		Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_FULLSCREEN)
		hide()
		
	elif game_mode == PLAY_MODE:
		game_mode = PAUSE_MODE
		player_object.game_mode = player_object.PAUSE_MODE
		Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		DisplayServer.window_set_mode(DisplayServer.WINDOW_MODE_WINDOWED)
		show()
